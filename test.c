#include "mpi.h"
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include "gather_impl.h"

long double g(long double x, int it)
{
	long double r = 0;
	int i;
	for (i = 0; i < it; i++) {
		r += tanhl(x)/it;
	}
	return r;
}

long double f(long double x, int it)
{
	long double r = 0;
	int i;
	for (i = 0; i < it; i++) {
		r += sinhl(x)/it;
	}
	return r;
}

int main(int argc, char **argv)
{
	MPI_Comm comm = MPI_COMM_WORLD;
	int gsize, i;
	char sendarray[100];
	int root = 0, myrank;
	char *rbuf;
	long double param;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(comm, &myrank);
	param = myrank;

	/* first gather */
	snprintf(sendarray, 100, "The hyperbolic sine of %Lf is %Lf, should be %LF.", param, f(param, 10000000), sinhl(param));
	if (myrank == root) {
		MPI_Comm_size(comm, &gsize);
		rbuf = (char *)malloc(gsize*100*sizeof(char));
		printf("\n-------------first test\n\n");
	}
	if (my_MPI_Gather(sendarray, 100, MPI_CHAR, rbuf, 100, MPI_CHAR, root, comm)) {
		printf("error occured\n");
		MPI_Finalize();
		return -1;
	}

	if (myrank == root)
		for (i = 0; i < gsize; i++)
			printf("%s\n", rbuf + 100*i);

	// MPI_Finalize();
	// return 0;

	/* second gather */
	root = 5;
	snprintf(sendarray, 100, "The hyperbolic tangent of %Lf is %Lf, should be %LF.", param, g(param, 10000000), tanhl(param));
	if (myrank == root) {
		MPI_Comm_size(comm, &gsize);
		rbuf = (char *)malloc(gsize*100*sizeof(char));
		printf("\n--------------second test\n\n");
	}
	if (my_MPI_Gather(sendarray, 100, MPI_CHAR, rbuf, 100, MPI_CHAR, root, comm)) {
		printf("error occured\n");
		MPI_Finalize();
		return -1;
	}

	if (myrank == root)
		for (i = 0; i < gsize; i++)
			printf("%s\n", rbuf + 100*i);

	MPI_Finalize();
	return 0;
}
